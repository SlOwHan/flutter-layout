import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart' show debugPaintSizeEnabled;

void main() {
  debugPaintSizeEnabled = false;
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter layout demo',
      home: buildHomePage('Strawberry Pavlova Recipe'),
    );
  }

  Widget buildHomePage(String title) {

    final titleText = Container(
      margin: EdgeInsets.only(left: 160, top: 10),
      height: 40,
      width: 500,
      color: Colors.blue,
      child:  Center(
        child: Text(
          'Strawberry Pavlova',
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.w800,
            letterSpacing: 2,
            fontSize: 20,
          ),
        ),
      ),
    );

    final subTitle = Container(
      margin: EdgeInsets.only(left: 160, top: 10),
      height: 50,
      width: 400,
      color: Colors.blue,
      child: Text(
        'Pavlova is a meringue-based dessert named after the Russian ballerina '
            'Anna Pavlova. Pavlova features a crisp crust and soft, light inside, '
            'topped with fruit and whipped cream.',
        textAlign: TextAlign.center,
        style: TextStyle(
          color: Colors.black,
          fontWeight: FontWeight.w800,
          fontFamily: 'Georgia',
          fontSize: 13,
        ),
      ),
    );


    var stars = Container(
      child:Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(Icons.star, color: Colors.black),
          Icon(Icons.star, color: Colors.black),
          Icon(Icons.star, color: Colors.black),
          Icon(Icons.star, color: Colors.black),
          Icon(Icons.star, color: Colors.black),
        ],
      ),

    );

    // #enddocregion stars

    final ratings = Container(

      margin: EdgeInsets.only(left: 160, top: 10),
      height: 60,
      width: 600,
      color: Colors.blue,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          stars,
          Text(
            '170 Reviews',
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.w800,
              fontFamily: 'Roboto',
              letterSpacing: 0.5,
              fontSize: 20,
            ),
          ),
        ],
      ),
    );

    final descTextStyle = TextStyle(
      color: Colors.black,
      fontWeight: FontWeight.w800,
      fontFamily: 'Roboto',
      letterSpacing: 0.5,
      fontSize: 18,
      height: 1,
    );


    final iconList = DefaultTextStyle.merge(
      style: descTextStyle,
      child: Container(
        margin: EdgeInsets.only(left: 160, top: 10),
        height: 70,
        width: 500,
        color: Colors.blue,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Column(
              children: [
                Icon(Icons.kitchen, color: Colors.green[500]),
                Text('PREP:'),
                Text('25 min'),
              ],
            ),
            Column(
              children: [
                Icon(Icons.timer, color: Colors.green[500]),
                Text('COOK:'),
                Text('1 hr'),
              ],
            ),
            Column(
              children: [
                Icon(Icons.restaurant, color: Colors.green[500]),
                Text('FEEDS:'),
                Text('4-6'),
              ],
            ),
          ],
        ),
      ),
    );

    final leftColumn = Container(
      padding: EdgeInsets.fromLTRB(10, 100, 100, 0),
      child: Column(
        children: [
          titleText,
          subTitle,
          ratings,
          iconList,
        ],
      ),
    );


    final mainImage = Image.asset(
      'images/lake.jpg',

      width: 600,
      height: 600,
    );

    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),

      body: Center(
        child: Container(
          margin: EdgeInsets.fromLTRB(0, 50, 0, 100),
          height: 800,
          child: Center(
            child: Card(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: 600,
                    child: leftColumn,
                  ),
                  mainImage,
                ],
              ),
            ),
          ),
        ),
      ),

    );
  }
}
